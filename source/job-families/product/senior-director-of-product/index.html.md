---
layout: job_family_page
title: "VP of Product"
---

As the VP of Product, you will play a key role in helping the GitLab Product Management team scale rapidly and realize our product vision to be a complete DevOps platform.

## Responsibilities
- Hire, lead, and coach a rapidly growing team of 30+ Product Managers
- Directly manage a team of 4-6 Product Directors
- Ensure a cohesive, coherent, and compelling end-to-end customer experience
- Align team with end-to-end product line vision and goals 
- Leverage portfolio product management techniques to ensure product investments are properly allocated across the end-to-end GitLab product   
- Partner effectively with Engineering, Design, and Product Marketing to ensure we validate, build, launch, and measure product experiences that customers love and value
- Help refine and implement the GitLab [product development flow](https://about.gitlab.com/handbook/product-development-flow/), ensuring team members receive training and coaching required to work effectively within the system
- Serve as a spokesperson for the end-to-end GitLab product internally and externally

## Requirements
- 18 years+ of relevant experience, with 10+ years of people management experience, including management of Directors+
- Strong understanding of DevOps markets, competition, and underlying technologies
- Track record of leading products to successful commercial outcomes
- Excellent at boardroom and big stage presentations, and able to inspire and motivate customers and employees through written and verbal communications
- Demonstrated ability to teach and coach the product management skills as outlined [here](https://about.gitlab.com/handbook/product/#product-management-career-development-framework)
